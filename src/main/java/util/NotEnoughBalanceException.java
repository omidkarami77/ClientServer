package util;

public class NotEnoughBalanceException extends Throwable {
    public NotEnoughBalanceException(){
        super("Your balance is not enough");
    }
}
