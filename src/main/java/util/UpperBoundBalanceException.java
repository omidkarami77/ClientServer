package util;

public class UpperBoundBalanceException extends Throwable {
    public UpperBoundBalanceException(String upperBalance){
        super("Your balance can not be more than " + upperBalance);
    }
}
